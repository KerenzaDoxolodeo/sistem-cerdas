from tkinter import messagebox, Canvas, StringVar, Frame, Tk, YES, BOTH, NW
from PIL import ImageTk
import AI
from random import randint
import Utils
from time import sleep
import testcode

class Damdas3Batu(object):    
    def create_oval(self, x, y, color):
        r = 25
        return self.canvas.create_oval(x, y, x + r, y + r, fill = color)

    def create_oval_given_index(self,index_X, index_Y, color):
        return (self.koordinat_X[index_X],
                self.koordinat_y[index_Y],
                self.create_oval(self.koordinat_X[index_X], self.koordinat_y[index_Y], color))

    def ai_run(self):
        _ , move = AI.analyze(self.matriks, 0, min(6, self.moveLeft), self.player, self.turn)
        result = Utils.check_legality_of_movement(self.matriks, move, True)
        self.player = (self.player == 1) + 1
        self.matriks = result
        if self.player == 1:
            self.turn+=1
        self.create_pion()
        self.canvas.update_idletasks()
        self.canvas.update()
        sleep(1)
        self.moveLeft -= 1
        return True

    def create_pion(self):
        for I in self.pion:
            self.canvas.delete(I[2])
        self.pion = []
        
        for I in range(3):
            for J in range(5):
                if self.matriks[J][I] == 1:
                    self.pion.append(self.create_oval_given_index(J, I, 'red'))
                elif self.matriks[J][I] == 2:
                    self.pion.append(self.create_oval_given_index(J, I, 'blue'))

    def human_run(self):
        '''
        Fungsi ini dipakai untuk menjalankan player.
        Setiap langkah dari player akan di cek apakah player sudah menentukan tujuan node tersebut.
        Akan dilakukan pengecekan juga terhadap node terpilih merupakan node player atau bukan.
        Dicek juga apakah node berangkat akan berbeda dengan node tujuan.
        Fungsi ini juga termasuk melakukan pengecekan apakah perpindahan player
        merupakan legal move.
        '''
        if self.nodeChosen[1] is None:
            return False
        if self.matriks[self.nodeChosen[0][0]][self.nodeChosen[0][1]] == self.AI_player:
            self.nodeChosen = [None, None]
            return False
        if tuple(self.nodeChosen[0]) == tuple(self.nodeChosen[1]):
            self.nodeChosen = [None, None]
            messagebox.showinfo("Information","Tujuan awal dan akhir tidak boleh sama")
            return False
        move = self.process_node()
        if move is None:
            self.nodeChosen = [None, None]
            return False
        result = Utils.check_legality_of_movement(self.matriks, (self.nodeChosen[0][0], self.nodeChosen[0][1], move) , True)
        if result is None:
            self.nodeChosen = [None, None]
            messagebox.showinfo("Information","Salah jalan")
            return False
        self.player = (self.player == 1) + 1
        self.matriks = result
        self.nodeChosen = [None, None]
        if (self.player == 1):
            self.turn += 1
        self.create_pion()
        self.canvas.update_idletasks()
        self.canvas.update()
        sleep(1)
        self.moveLeft -= 1
        return True

    def run_game(self,turn):
        if Utils.check_victory(self.matriks,turn) == "Belum ada pemenang":
            self.canvas.create_rectangle(25,3,68,20, fill='white')
            printString = "Turn " + str(turn)
            self.canvas.create_text(45,10, text = printString)
            if self.player == self.AI_player:
                if self.ai_run():
                    self.run_game(turn)
            else:
                if self.human_run():
                    self.run_game(turn)
        else :
            messagebox.showinfo("Pemenangnya adalah ...",Utils.check_victory(self.matriks,turn))

    def process_node(self):
        if self.nodeChosen[1] is None:
            return None
        self.dictionaryPosition = { (1,0) : 0, (1, -1) : 1, (0, -1) : 2,
                                    (-1, -1) : 3, (-1, 0) : 4, (-1, 1): 5,
                                    (0, 1) : 6, (1, 1): 7 }
        deltaX = self.nodeChosen[1][0] - self.nodeChosen[0][0]
        deltaY = self.nodeChosen[0][1] - self.nodeChosen[1][1]
        if (deltaX, deltaY) in self.dictionaryPosition:
            return self.dictionaryPosition[(deltaX, deltaY)]
        return None

    def __init__ (self):
        self.canvas = None
        self.koordinat_X = [25, 98, 172, 245, 320]
        self.koordinat_y = [25, 100, 175]
        self.player = 1
        self.turn = 1
        self.res = 0
        self.moveLeft = 40
        self.AI_player = randint(1, 2)
        self.pion = None
        self.nodeChosen = [None, None]
        self.matriks = [[0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0]]
        self.matriks = testcode.initiate(self.matriks)
        window = Tk()
        window.title("Damdas 3 Batu") # Set a title
        frame1 = Frame(window) # Create and add a frame to window
        frame1.pack()
        self.v1 = StringVar()
        canvas = Canvas(window, width = 370,height=220,bg = "white")
        canvas.pack(expand = YES, fill = BOTH)
        self.canvas = canvas
        image = ImageTk.PhotoImage(file= "gambar.jpg")
        canvas.create_image(30, 30, image = image, anchor = NW)
        r = 25
        self.pion = [self.create_oval_given_index(1,0, 'red'),
                    self.create_oval_given_index(1,1, 'red'),
                    self.create_oval_given_index(1,2, 'red'),
                    self.create_oval_given_index(3,0, 'blue'),
                    self.create_oval_given_index(3,1, 'blue'),
                    self.create_oval_given_index(3,2, 'blue')]
        self.canvas = canvas
        self.canvas.pack()
        self.canvas.bind("<ButtonPress-1>",self.selectShape) # left click
        self.drawn = None
        if self.AI_player == 1:
            messagebox.showinfo("Start of Game", "Anda akan main kedua dengan pion biru.\n Kalahkan lawan dalam 20 turn!")
        if self.AI_player == 2:
            messagebox.showinfo("Start of Game", "Anda akan main pertama dengan pion merah.\n Kalahkan lawan dalam 20 turn!")
        self.canvas.after(1000, self.run_game(self.turn))
        window.mainloop()

    def searchPosition(self,x,y):
        bestX=1000
        bestY=1000
        posX = 1000
        posY = 1000
        for i in self.koordinat_X:
            if abs(i - x) < bestX:
                bestX = abs(i - x)
                posX = i
        for i in self.koordinat_y:
            if abs(i - y) < bestY:
                bestY = abs(i - y)
                posY = i
        positionArray = [self.koordinat_X.index(posX),self.koordinat_y.index(posY)]
        return positionArray
   
    # define function to select shape
    def selectShape(self,event):
        if self.player != self.AI_player:
            result = self.searchPosition(int(event.x),int(event.y))
            if self.nodeChosen[0] is None:
                self.nodeChosen[0] = result
            else:
                self.nodeChosen[1] = result
            self.run_game(self.turn)

if __name__ == '__main__':
    Damdas3Batu()
