import Utils
import testcode

def heuristic(node, player, turn):

    # Attempt at making the AI more aggresive (Higher reward for playing aggresively, hopefully preventing stallings)
    if player == 1:
        opponent_pawn = 0
        opponent_pawn += sum([I == 2 for I in node[2]]) * 1
        opponent_pawn += sum([I == 2 for I in node[1]]) * 3
        opponent_pawn += sum([I == 2 for I in node[0]]) * 5
        my_pawn = 0
        my_pawn += sum([I == 1 for I in node[2]]) * 1
        my_pawn += sum([I == 1 for I in node[3]]) * 5
        my_pawn += sum([I == 1 for I in node[4]]) * 9
        my_pawn += sum([I == 1 for I in node[0]]) * -999
        return my_pawn - opponent_pawn
    else:
        opponent_pawn = 0
        opponent_pawn += sum([I == 2 for I in node[2]]) * 1
        opponent_pawn += sum([I == 2 for I in node[1]]) * 5
        opponent_pawn += sum([I == 2 for I in node[0]]) * 9
        opponent_pawn += sum([I == 2 for I in node[4]]) * -999
        my_pawn = 0
        my_pawn += sum([I == 1 for I in node[2]]) * 1
        my_pawn += sum([I == 1 for I in node[3]]) * 3
        my_pawn += sum([I == 1 for I in node[4]]) * 5
        return my_pawn - opponent_pawn
    

def analyze(node, depth, maximum_depth, mode, turn):
    '''
        mode 1 , pemain 1 sedang gerak -- max
        mode 2 , pemain 2 sedang gerak -- min
    '''

    if depth == maximum_depth:
        return (heuristic(node,mode,turn), None)
    result = Utils.check_victory(node,turn)
    if result == "Kedua menang":
        return (- 200 , None)
    elif result == "Pertama menang":
        return (200 , None)
    elif result == "Permainan Seri":
        if mode == 1:
            return(100,None)
        else:
            return(-100,None)
    legal_moves = {}
    # Generate aturan
    for X in range(5):
        for Y in range(3):
            if node[X][Y] == mode:
                for movement in range(8):
                    result = Utils.check_legality_of_movement(node, (X,Y,movement), False)
                    if result is not None:
                        legal_moves[(X, Y, movement)] = result

    top_move = None
    if mode == 1:
        top_score = -99999
    else:
        top_score = 99999
    for I in legal_moves:
        result, _ = analyze(legal_moves[I],  depth + 1, maximum_depth, (mode == 1) + 1, turn)
        if mode == 1:
            if result > top_score:
                top_score = result
                top_move = I
        else:
            if result < top_score:
                top_score = result
                top_move = I

    return (top_score, top_move)

if __name__ == '__main__':
    print("~~~")
    matrix = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
    matrix = testcode.initiate(matrix)

    player = 1
    turn = 1
    res = 0
    while Utils.check_victory(matrix,turn) == "Belum ada pemenang":
        _ , move = analyze(matrix, 0, 6, player, turn)
        result = Utils.check_legality_of_movement(matrix, move, True)
        '''
        1 2 3 | 5 6 7
        4 - 6 | 4 - 0
        7 8 9 | 3 2 1
        '''
        player = (player == 1) + 1
        matrix = result
        testcode.illustrate(matrix)
        if(player%2 == 1):
            turn+=1

    print (Utils.check_victory(matrix,turn))