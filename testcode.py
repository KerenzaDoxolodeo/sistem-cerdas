print("  ====== DamDas 3 Batu ======")
# "[]-\ []---[]---[] /-[]"
# "    \\ | \\    / | / "
# "[]---[]---[]---[]---[]"
# "    / | /    \\ | \\ "
# "[]-/ []---[]---[] \-[]"

# Initiate permainan, set pion2 di posisi awal
def initiate(nodes):
    nodes[1][0] = 1
    nodes[1][1] = 1
    nodes[1][2] = 1
    nodes[3][0] = 2
    nodes[3][1] = 2
    nodes[3][2] = 2
    return nodes

# Hardcode print untuk memunculkan papan permainan
def illustrate(nodes):
    gamemap = " c 0      1     2     3      4 \n"
    gamemap += " 0 [" + str(int(nodes[0][0])) + "]-\ [" + str(int(nodes[1][0])) + "]---[" + str(int(nodes[2][0])) + "]---[" + str(int(nodes[3][0])) + "] /-[" + str(int(nodes[4][0])) + "] \n"
    gamemap += "    |   \\ |  /  |  \\  | /   |\n"
    gamemap += " 1 [" + str(int(nodes[0][1])) + "]---[" + str(int(nodes[1][1])) + "]---[" + str(int(nodes[2][1])) + "]---[" + str(int(nodes[3][1])) + "]---[" + str(int(nodes[4][1])) + "] \n"
    gamemap += "    |   / |  \\  |  /  | \\   | \n"
    gamemap += " 2 [" + str(int(nodes[0][2])) + "]-/ [" + str(int(nodes[1][2])) + "]---[" + str(int(nodes[2][2])) + "]---[" + str(int(nodes[3][2])) + "] \-[" + str(int(nodes[4][2])) + "] \n"
    print(gamemap)

    # Move pion, tukar 2 buah elemen di matrix, prinsipnya menggunakan case
def move(x,y,dir,nodes):
    if(validcheck(x,y,dir,nodes)):
        if(dir == 1):
            nodes[x-1][y+1] = nodes [x][y]
            nodes[x][y] = 0
        elif(dir ==2):
            nodes[x][y+1] = nodes [x][y]
            nodes [x][y] = 0
        elif(dir ==3):
            nodes[x+1][y+1] = nodes [x][y]
            nodes[x][y] = 0
        elif(dir == 4):
            nodes[x-1][y] = nodes [x][y]
            nodes [x][y] = 0
        elif(dir ==6):
            nodes[x+1][y] = nodes [x][y]
            nodes [x][y] = 0
        elif(dir ==7):
            nodes[x-1][y-1] = nodes [x][y]
            nodes [x][y] = 0
        elif(dir == 8):
            nodes[x][y-1] = nodes [x][y]
            nodes [x][y] = 0
        elif(dir ==9):
            nodes[x+1][y-1] = nodes [x][y]
            nodes [x][y] = 0
        print("")
        print("==========  Move  ===========")
        illustrate(nodes)
    return

def validcheck(x,y,dir,nodes):
    inv = "Invalid Move"
    val = False
    if(nodes[x][y] == 0):
        print(inv)
        return val
    else:
        if(dir == 1):
            if(nodes[x-1][y+1] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (1,1):True,
                    (2,0):True,
                    (4,0):True,
                    (3,1):True
                    }.get((x,y),False)
                val = switch(x,y)
        elif(dir == 2):
            if(nodes[x][y+1] != 0):
                print(inv)
                return val
            elif(x<5 and x >=0 and y < 2 and y >= 0):
                val = True
                return val
        elif(dir == 3):
            if(nodes[x+1][y+1] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (0,0):True,
                    (2,0):True,
                    (1,1):True,
                    (3,1):True
                    }.get((x,y),False)
                val = switch(x,y)
        elif(dir == 4):
            if(nodes[x-1][y] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (2,0):True,
                    (3,0):True,
                    (1,1):True,
                    (2,1):True,
                    (3,1):True,
                    (4,1):True,
                    (2,2):True,
                    (3,2):True
                    }.get((x,y),False)
                val = switch(x,y)
        elif(dir == 6):
            if(nodes[x+1][y] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (1,0):True,
                    (2,0):True,
                    (0,1):True,
                    (1,1):True,
                    (2,1):True,
                    (3,1):True,
                    (1,2):True,
                    (2,2):True
                    }.get((x,y),False)
                val = switch(x,y)
        elif(dir == 7):
            if(nodes[x-1][y-1] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (4,2):True,
                    (1,1):True,
                    (2,2):True,
                    (3,1):True
                    }.get((x,y),False)
                val = switch(x,y)
        elif(dir == 8):
            if(nodes[x][y-1] != 0):
                print(inv)
                return val
            elif(x<5 and x >=0 and y <= 2 and y > 0):
                val = True
                return val
        elif(dir == 9):
            if(nodes[x+1][y-1] != 0):
                print(inv)
                return val
            else:
                def switch(x,y):
                    return{
                    (0,2):True,
                    (1,1):True,
                    (2,2):True,
                    (3,1):True
                    }.get((x,y),False)
                val = switch(x,y)
    if val:
        return val
    else:
        print(inv)
        return val

# nodes = np.zeros((5,3))
# nodes = initiate(nodes)
# illustrate(nodes)

# # Demo
# # Uncomment this part to try it

# # get 2 out of the way
# move(3,1,1,nodes)

# # move (1,1) to finish
# move(1,1,6,nodes)
# move(2,1,6,nodes)
# move(3,1,6,nodes)

# # Move (1,0) to finish
# move(1,0,6,nodes)
# move(2,0,3,nodes)
# move(3,1,9,nodes)

# # Move (1,2) to finish
# move(1,2,8,nodes)
# move(1,1,9,nodes)
# move(2,0,3,nodes)
# move(3,1,3,nodes)


