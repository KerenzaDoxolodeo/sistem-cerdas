def check_victory(matrix,turn):
    '''
    Fungsi dipakai untuk memeriksa apakah dalam state board sekarang
    sudah ada pemenangnya.
    input:
        sebuah array 3 x 5 menunjukan posisi. Elemen 0 => Kosong
        Elemen 1 => Milik pemain yang maju pertama. Dia mulai dari kiri dan berjalan ke kanan.
        Elemen 2 => Milik pemain yang maju kedua. Dia mulai dari kanan dan berjalan ke kiri.
    output:
        sebuah string yaitu "Belum ada pemenang" atau "Pertama menang"
        atau "Kedua menang" jika turn sampai 20 maka akan ditentukan dari berapa banyak node yang sudah sampai
        finish, jika sama maka "Permainan Seri"

     contoh:
        input:
            - matrix : [[-1,-1,0], [0,-1,0], [0,1,1], [0,1,0],[0,0,0]]
        output:
            - "Kedua menang"
    '''
    if (turn == 21):
        count = 0

        if (matrix[4][0] == 1):
            count +=1
        if (matrix[4][1] == 1):
            count +=1
        if (matrix[4][2] == 1):
            count +=1
        if (matrix[0][0] == 2):
            count -=1
        if (matrix[0][1] == 2):
            count -=1
        if (matrix[0][2] == 2):
            count -=1
        if (count>0):
            return "Pertama menang"
        elif (count<0):
            return "Kedua menang"
        else:  
            return "Permainan Seri"

    if ((matrix[4][0] == 1) + (matrix[4][1] == 1) + (matrix[4][2] == 1)) == 3:
        return "Pertama menang"
    
    if ((matrix[0][0] == 2) + (matrix[0][1] == 2) + (matrix[0][2] == 2)) == 3:
        return "Kedua menang"
    
    return "Belum ada pemenang"

def check_legality_of_movement(matrix, position, verbose):
    '''
    Cek apakah sebuah movement valid
    
    Fungsi dipakai untuk memeriksa apakah sebuah movement itu valid
    input:
        matrix : sebuah array 3 x 5 yang menunjukan keadaan papan
        posiition : sebuah tuple berisi empat element elemen
            elemen 0 dan 1 menunjukan koordinat pion yang ingin dimainkan (ie. matrix[position[0]][position[1]])
            elemen 2 menunjukan posisi arah pergerakan pion. 
                - 0 artinya bergerak ke arah timur
                - 1 artinya bergerak ke arah tenggara
                - 2 artinya bergerak ke arah selatan
                dst hingga 7 artinya bergerak ke arah timur laut
    output:
        jika valid, matrix setelah posisi pion itu bergerak
        jika tidak valid, return None
    '''

    # Cek apakah di posisi tersebut ada pion
    if matrix[position[0]][position[1]] == 0:
        if verbose:
            print("A")
        return None
    
    # # Pion yang sudah sampai di segitiga tidak boleh digerakan lagi
    # if position[0] == 4:
    #     if matrix[4][position[1]] == 1:
    #         return None
    # if position[0] == 0:
    #     if matrix[0][position[1]] == 2:
    #         return None
    
    # Konversi posisi menjadi delta x dan y
    if position[2] in [0,1,7]:
        delta_X = -1 # Ke kanan
    elif position[2] in [3,4,5]:
        delta_X = 1 # Ke kiri
    else:
        delta_X = 0
    
    if position[2] in [5,6,7]:
        delta_Y = 1 # Ke atas
    elif position[2] in [1,2,3]:
        delta_Y = -1 # Ke bawah
    else:
        delta_Y = 0
    
    # Cek apkah di pergerakan tersebut akan keluar panan
    if not (0 <= position[0] - delta_X <= 4): 
        if verbose:
            print("B")
        return None
    if not (0 <= position[1] - delta_Y <= 2): 
        if verbose:
            print("C")
        return None
    
    # Cek apakah di tempat destinasi, ada pion lain
    if matrix[position[0] - delta_X][position[1] - delta_Y] != 0:
        if verbose:
            print("D")
        return None
    
    legal_move = [ [1, 2], [0, 2, 6], [6,7],
                [0, 2], [0, 1, 2, 3, 4, 5, 6, 7], [0, 6],
                [0, 1, 2, 3, 4], [0 ,2 ,4 , 6], [0, 4, 5, 6, 7],
                [2, 4], [0, 1, 2, 3, 4, 5, 6, 7], [4,6],
                [2, 3], [2, 4, 6], [5,6]
            ]
    
    if position[2] not in legal_move[position[0] * 3 + position[1]]:
        if verbose:
            print("E")
        return None
    # Pada titik ini, kita tahu movementnya itu valid
    matriks = [x[:] for x in matrix]
    token = matriks[position[0]][position[1]]
    matriks[position[0]][position[1]] = 0
    matriks[position[0] - delta_X][position[1] - delta_Y] = token
    return matriks

array =  [[0, 0, 0], [0, 1, 0], [1, 2, 1], [2, 0, 2], [0, 0, 0]]